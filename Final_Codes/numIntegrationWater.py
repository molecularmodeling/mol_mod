import numpy as np
import matplotlib.pyplot as plt
import trajectoryIO
import integrationSchemes
import forces
import time
#This script is used to run the simulation for a box of water molecules	
#---------------------------Main Script----------------#
eqbmLength = 0.9572
eqbmAngle = (np.pi*104.52)/180
kL = 5024.16
kT = 6.2802
tFinal = 1000
delT = 0.02
atomicityWater = 3
sigma = np.array([3.15061,0.0,0.0])
eps = np.array([0.66386,0.0,0.0])
mWater = np.array([16,1,1])
bondsWater = ["O","H","H"]




#-----------Initialize water molecules--------#
start_timing = time.time()

q_init = np.array([[2, 1, 0],
     [2+eqbmLength, 1, 0],
     [2+eqbmLength*np.cos(eqbmAngle), 1+eqbmLength*np.sin(eqbmAngle), 0]])
     
L = 50 #Size of the box in Angstroms
q = trajectoryIO.initializePositions(q_init,L,'Water',0.2,3)
nAtoms = q.shape[0]
nMols = int(nAtoms/atomicityWater)


#Initial  conditions- normal distribution for velocity
k = 0.166
T = 300
T = T/120.27
mean = 0
m_vec = np.tile(mWater,nMols)
sigma_speed = np.sqrt((k*T)/m_vec)
#v = np.zeros((nAtoms,3))
v = np.vstack((np.vstack((np.random.normal(mean,sigma_speed),np.random.normal(mean,sigma_speed))),np.random.normal(mean,sigma_speed))).T

timeSteps = int((tFinal-0.0)/delT)
file = 0
f = 0
temp = np.zeros(timeSteps)
Eb = np.zeros(timeSteps)
Ea = np.zeros(timeSteps)
Elj = np.zeros(timeSteps)
Ek = np.zeros(timeSteps)
#Integration
for i in range(timeSteps):
	if (i%1000 == 0):
		print("Time step: ",i,"\n")
	file = trajectoryIO.writeTrajectory('Watertest.xyz',q,atomicityWater,i*delT,timeSteps,bondsWater,file)
 	for j in range(0,nAtoms,atomicityWater):
 		Eb[i] += forces.bpot(q[j]-q[j+1],eqbmLength,kL)
 		Eb[i] += forces.bpot(q[j]-q[j+2],eqbmLength,kL)
 		Ea[i] += (kT/2)*(forces.angle(q[j+1]-q[j],q[j+2]-q[j])-eqbmAngle)**2
 		for ii in range(j+3,nAtoms,atomicityWater):
 			dist = (q[ii]-q[j]) - L*np.floor((q[ii]-q[j])/L+0.5)
 			temporary = (sigma[0]/np.linalg.norm(dist))**6
			Elj[i] += 4*eps[0]*(temporary**2-temporary)
 	Ek[i] += np.sum(forces.kineticEnergy(v,mWater))

        
    
	Tnew = forces.temperature(v,k,mWater)
	v = np.sqrt(T/Tnew)*v
	temp[i] = Tnew*120.27
	[q,v,f] = integrationSchemes.velVerletIntWater(q,v,atomicityWater,mWater,kT,eqbmLength,kL,eqbmAngle,eps,sigma,tFinal,delT,L,f,i)
	
#Total energy
E = Eb+Ea+Elj+Ek
#Necessary Energy Plots
plt.figure(1)
plt.plot(1e-4*np.linspace(0,timeSteps*delT,timeSteps),temp)
plt.xlabel("Time (ns)")
plt.ylabel("Temperature (K)")
plt.figure(2)
plt.plot(1e-4*np.linspace(0,timeSteps*delT,timeSteps),E)
plt.xlabel("Time (ns)")
plt.ylabel("Total energy (kJ/mol)")
plt.show()
    

#-------------------End of main script-------------#
