import numpy as np
"""
The trajectoryIO.py file contains the modules used 
to read/write trajectories used for the simulations.
"""
def readTrajectory(trajFile):
"""
This module is used to read the number of atoms and their position vectors from a .xyz file.
Input: <filename>.xyz in quotes
Output: trajectory- a matrix of size (N*3) of position vectors of each atom
		N- number of atoms
"""
    trajectory=[]
    with open(trajFile, "r") as tF:
        line = tF.readline()
        while line is not "":
            #first line is number of atoms
            N = int(line.strip())
            tF.readline().strip() # second line is a comment that is not needed
            q = []
            for i in range(N):
                line = tF.readline().strip().split(" ")
                for c in line[1:]:
                    if c is not "":
                        q.append(float(c))
            trajectory.append(np.array(q))

            line = tF.readline()

    return trajectory, N



def writeTrajectory(trajFile,q,atomicity,timeStep,stop,molMap,file):
"""
This module is used to write position vectors of the atoms of a set of similar molecules to a .xyz file at any given timestep.
Input: <filename>.xyz in quotes
	   q- the position vector matrix of all the atoms
       atomicity- number of atoms per molecule
       time step - timestep of the simulation
       stop- final timestep of the simulation
       molMap- a list of the names of atoms of the molecule
       file- file object
Output: file object
"""
    if timeStep == 0 :
        file = open(trajFile, 'w')
    
    file.write('%s\n' % (np.shape(q)[0]))
    file.write('Time step: %s\n' % (timeStep))
    for i in range(np.shape(q)[0]):
        file.write('%s %8s %8s %8s\n' % (molMap[i%atomicity],q[i,0],q[i,1],q[i,2]))
        
    if timeStep == stop-1 :
        file.close()
        
    return file

def writeTrajectoryMix(trajFile,q,timeStep,stop,molMap,file):
"""
This module is used to write position vectors of the atoms of a set of dissimilar molecules to a .xyz file at any given timestep.
Input: <filename>.xyz in quotes
	   q- the position vector matrix of all the atoms
       time step - timestep of the simulation
       stop- final timestep of the simulation
       molMap- a list of the names of atoms of the molecules the order in which they are present in `q'
       file- file object
Output: file object
"""
	if timeStep == 0 :
		file = open(trajFile, 'w')

	file.write('%s\n' % (np.shape(q)[0]))
	file.write('Time step: %s\n' % (timeStep))
	for i in range(np.shape(q)[0]):
		print(i)
		file.write('%s %8s %8s %8s\n' % (molMap[i],q[i,0],q[i,1],q[i,2]))
        
	if timeStep == stop-1 :
		file.close()
        
	return file          
            
def initializePositions(q,L,molType,molDensity,nDimensions):
"""
This module is used to initialise position vectors of the atoms of a set of similar molecules in a matrix `qn'. The position vectors of the atoms of a single molecule that is close to the origin is given as an input and this configuration is copied as a lattice by offsetting it by a distance that is computed based on the density and largest distance between any two atoms of he given molecule
Input: 
	   q- the position vector matrix of the atoms of a single molecule
       L - dimension of the box used in the simulation
       molType- the name of the molecule as a string
       molDensity- used to set the density of the molecules in the box. Use 0.55 for a box full of molecules
       nDimensions- 1(line)/2(square)/3(cube) dimension initialization
Output: qn - a matrix containing the position vectors of all the initialized molecules
"""

	atomicity = q.shape[0]
	distanceFactor = 1/molDensity
	if (molType == 'Water'):
		nMols = int(molDensity*L//(np.linalg.norm(q[1]-q[2])))	#q1 and q2 are positions of the hydrogens which are the farthest 
		offset = distanceFactor*np.linalg.norm(q[1]-q[2])
	else:
		nMols = int(molDensity*L//(np.linalg.norm(q[4]-q[8])))
		offset = distanceFactor*np.linalg.norm(q[4]-q[8])	#q4 and q8 are the positions of atoms that are the farthest in the ethanol mol.
	it = 0
	if (nDimensions == 2):
		qn = np.zeros((nMols*(nMols)*atomicity,3))
		for i in range(0,nMols):
			for j in range(0,nMols):
				qn[it:it+atomicity] = q + np.array([i*offset,j*offset,0])
				it = it+atomicity
	elif (nDimensions == 1):
		qn = np.zeros(((nMols)*atomicity,3))
		for i in range(0,nMols):
			qn[it:it+atomicity] = q + np.array([i*offset,0,0])
			it = it+atomicity
	else:
		qn = np.zeros((nMols*nMols*nMols*atomicity,3))
		for i in range(0,nMols):
			for j in range(0,nMols):
				for k in range(0,nMols):
					qn[it:it+atomicity] = q + np.array([i*offset,j*offset,k*offset])
					it = it+atomicity
	
	
	return qn          
	
def initializeMixture(qWater, qEthanol, nMols, L, nDimensions):
"""
This module is used to initialise position vectors of the atoms of a set of two different molecules in a matrix `qn'. The position vectors of the atoms of a single molecule that are close to the origin are given as an input and this configuration is copied as a lattice by offsetting it by a distance that is computed based on the density and largest distance between any two atoms of he given molecules.
Input: 
	   qWater/Ethanol- the position vector matrix of the atoms of a single molecule
       L - dimension of the box used in the simulation
       nMols- total number of molecules to be placed inside the domain
       nDimensions- 1(line)/2(square)/3(cube) dimension initialization
Output: qn - a matrix containing the position vectors of all the initialized molecules with all the ethanol molecules followed by all the water molecules.
		nEmols- number of ethanol molecules generated
        nWmols- number of water molecules generated

Note: The total number of molecules in the output may be less than the requested number due to the rounding off due to calculations. 
"""
	nEmols = int(np.ceil(0.135*nMols))
	nWmols = nMols - nEmols
	eAtomicity = qEthanol.shape[0]
	wAtomicity = qWater.shape[0] 
	cell = np.linalg.norm(qEthanol[4] - qEthanol[8])
	itE = 0
	itW = 0
	if (nDimensions == 2):
		nEmols = int(np.sqrt(nEmols))
		nWmols = int(np.sqrt(nWmols))
		qnE = np.zeros((nEmols*(nEmols)*eAtomicity,3))
		qnW = np.zeros((nWmols*(nWmols)*wAtomicity,3))
		offsetE = cell*(np.floor(L/cell)/nEmols)
		offsetW = cell*(np.floor(L/cell)/nWmols)
		#Ethanol lattice
		for i in range(0,nEmols):
			for j in range(0,nEmols):
				qnE[itE:itE+eAtomicity] = qEthanol + np.array([i*offsetE,j*offsetE,0])
				itE = itE+eAtomicity
		#Water Lattice
		for i in range(0,nWmols):
			for j in range(0,nWmols):
				qnW[itW:itW+wAtomicity] = qWater + np.array([i*offsetW,j*offsetW,0])
				itW = itW+wAtomicity
		nEmols = nEmols**2
		nWmols = nWmols**2
	elif (nDimensions == 1):
		qnE = np.zeros(((nEmols)*eAtomicity,3))
		qnW = np.zeros(((nWmols)*wAtomicity,3))
		offsetE = cell*(np.floor(L/cell)/nEmols)
		offsetW = cell*(np.floor(L/cell)/nWmols)
		#Ethanol lattice
		for i in range(0,nEmols):
			qnE[itE:itE+eAtomicity] = qEthanol + np.array([i*offsetE,0,0])
			itE = itE+eAtomicity
		#Water lattice
		for i in range(0,nWmols):
			qnW[itW:itW+wAtomicity] = qWater + np.array([i*offsetW,0,0])
			itW = itW+wAtomicity
	else:
		nEmols = int(np.ceil(np.cbrt(nEmols)))
		nWmols = int(np.ceil(np.cbrt(nWmols)))
		qnE = np.zeros((nEmols*nEmols*nEmols*eAtomicity,3))
		qnW = np.zeros((nWmols*nWmols*nWmols*wAtomicity,3))
		offsetE = cell*(np.floor(L/cell)/nEmols)
		offsetW = cell*(np.floor(L/cell)/nWmols)
		#Ethanol lattice
		for i in range(0,nEmols):
			for j in range(0,nEmols):
				for k in range(0,nEmols):
					qnE[itE:itE+eAtomicity] = qEthanol + np.array([i*offsetE,j*offsetE,k*offsetE])
					itE = itE+eAtomicity
		#Water Lattice
		for i in range(0,nWmols):
			for j in range(0,nWmols):
				for k in range(0,nWmols):
					qnW[itW:itW+wAtomicity] = qWater + np.array([i*offsetW,j*offsetW,k*offsetW])
					itW = itW+wAtomicity
		nEmols = nEmols**3
		nWmols = nWmols**3	
	return [np.append(qnE,qnW,axis=0),nEmols,nWmols] 
