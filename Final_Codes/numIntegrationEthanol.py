import numpy as np
import matplotlib.pyplot as plt
import trajectoryIO
import integrationSchemes
import forces
#This script is used to run the simulation for simulating a box of ethanol molecules	
#---------------------------Main Script----------------#
 
#This connectivity matrix is based on the initial ordering of the initial conditions file.
connectivityMatrix = [[1,3,4,5],[0,2,6,7],[1,8],[0],[0],[0],[1],[1],[2]]

#Eqbm length combinations- based on the connectivity matrix
eqbmLength = [10*np.array([0.1529,0.1090,0.1090,0.1090]),10*np.array([0.1529,0.1410,0.1090,0.1090]),10*np.array([0.1410,0.0945]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.0945])]
kL = [0.01*np.array([224262.4,284512.0,284512.0,284512.0]),0.01*np.array([224262.4,267776.0,284512.0,284512.0]),0.01*np.array([267776.0,462750.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([462750.0])]
aclist = [[0,1],[0,2],[0,3],[0,0],[1,2],[1,3],[1,1],[2,1]]#This is not the arc list. This is a matrix that stores the indices of the atoms in the connectivity matrix
#Eqbm angle- based on the connectivity matrix- element- pair-wise combinations
eqbmAngle = [[108.5,108.5,108.5,107.8,107.8,107.8],[109.5,110.7,110.7,109.5,109.5,107.8],[108.5]]
kT = [0.01*np.array([292.88,292.88,292.88,276.144,276.144,276.144]),0.01*np.array([414.4,313.8,313.8,292.88,292.88,276.144]),0.01*np.array([460.240])]
#Dihedral Data- yet to change to reduced units
dihedralConMat = [['3 0 1 6','4 0 1 6','5 0 1 6','3 0 1 7','4 0 1 7','5 0 1 7'],['3 0 1 2','4 0 1 2','5 0 1 2'],['0 1 2 8'],['6 1 2 8','7 1 2 8']]
dihedralData = [[0.62760,1.88280,0.0,-3.91622],[0.97905,2.93716,0.0,-3.91622],[-0.4431,3.8255,0.72801,-4.11705],[0.94140,2.82420,0.0,-3.76560]]
#Lennard Jones Potential Data
sigma = 10*np.array([0.35,0.35,0.312,0.25,0.25,0.25,0.25,0.25,0.0])
eps = 0.1*np.array([2.76144,2.76144,7.11280,1.25520,1.25520,1.25520,1.25520,1.25520,0.00])

tFinal = 1000 
delT = 0.02 
atomicityEthanol = 9
mEthanol = np.array([12,12,16,1,1,1,1,1,1])
bondsEthanol = ["C","C","O","H","H","H","H","H","H"]
t,nAtoms = trajectoryIO.readTrajectory('Ethanolt0.xyz')

#Initializing a lattice of ethanol molecules
qInit = t[0].reshape(nAtoms,3)
L = 20	#Box length
qInit = qInit + 2*np.repeat(np.array([[1,1.5,1]]),atomicityEthanol,axis=0)

q = trajectoryIO.initializePositions(qInit,20,'Ethanol',0.6,3)
nAtoms = q.shape[0]
nMols = int(nAtoms/atomicityEthanol)

#Initial  conditions for velocity
k = 0.166
T = 300
T = T/120.27
mean = 0
m_vec = np.tile(mEthanol,nMols)
sigma_speed = np.sqrt((k*T)/m_vec)
v = np.vstack((np.vstack((np.random.normal(mean,sigma_speed),np.random.normal(mean,sigma_speed))),np.random.normal(mean,sigma_speed))).T


print('Start simulation!')
timeSteps = int((tFinal-0.0)/delT)
ke = np.zeros(timeSteps)	#Initialize kinetic energy
P = 0 #Initialize potential energy argument
p = np.zeros(timeSteps)	#Initialize bonded-potential energy
Elj = np.zeros(timeSteps) #Initialize Lennard Jones' potential
temp = np.zeros(timeSteps) #Initialize temperature
file = 0	#Initialize file object for I/O
f = 0		#Initialize force argument
#Integration start
for i in range(timeSteps):
	print("Time step: ",i,"\n")
	ke[i] = sum(forces.kineticEnergy(v,mEthanol))
	file = trajectoryIO.writeTrajectory('wrEthanol.xyz',q,atomicityEthanol,i*delT,timeSteps,bondsEthanol,file)
    #Computing the lennard Jones potential for the given molecules- q
	for j in range(0,nAtoms,atomicityEthanol):
		for ii in range(j+atomicityEthanol,nAtoms,atomicityEthanol):
			dqTemp = q[ii:ii+atomicityEthanol] - q[j:j+atomicityEthanol,np.newaxis]
			dq = np.reshape(dqTemp,[dqTemp.shape[0]*dqTemp.shape[1],3])
			sigmaTemp = (sigma + sigma[:,np.newaxis])/2
			sigmaMix = np.reshape(sigmaTemp,sigmaTemp.shape[0]*sigmaTemp.shape[1])
			epsTemp = np.sqrt(eps * eps[:,np.newaxis])
			epsMix = np.reshape(epsTemp,epsTemp.shape[0]*epsTemp.shape[1])
			
			dist = (dq) - L*np.floor(dq/L+0.5)
			temporary = (np.divide(sigmaMix,np.linalg.norm(dist,axis=1)))**6
			Elj[i] = sum(4*epsMix*(temporary**2-temporary))
    #Temperature to be used for thermostat scaling
	temp[i] = forces.temperature(v,k,mEthanol)
    #Velocity rescaling
	v = np.sqrt(T/temp[i])*v
    #Calling the main integrator function by passing all the necessary arguments
	[q,v,f,P] = integrationSchemes.velVerletIntEthanol(q,v,atomicityEthanol,mEthanol,connectivityMatrix,eqbmLength,kL,aclist,eqbmAngle,kT,eps,sigma,dihedralConMat,dihedralData,f,P,i,delT,L)
	p[i] = sum(P)
#-----------End of integration--------#
#Plotting necessary data
time = np.linspace(0,tFinal,timeSteps)*1e-4
plt.figure(1)
plt.plot(time,ke)
plt.xlabel('time(ns)')
plt.ylabel('Kinetic Energy(kJ/mol)')
plt.show()
plt.figure(2)
plt.plot(time,p)
plt.xlabel('time(ns)')
plt.ylabel('Potential Energy(kJ/mol)')
plt.show()
plt.figure(3)
plt.plot(time,(p+k+Elj))
plt.xlabel('time(ns)')
plt.ylabel('Total Energy(kJ/mol)')
plt.show()
plt.plot(4)
plt.plot(time,temp*120.27)
plt.xlabel('time(ns)')
plt.ylabel('Temperature(K)')
plt.show()


#-------------------End of main script-------------#
