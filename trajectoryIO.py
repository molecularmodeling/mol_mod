import numpy as np
def readTrajectory(trajFile):

    trajectory=[]
    with open(trajFile, "r") as tF:
        line = tF.readline()
        while line is not "":
            #first line is number of atoms
            N = int(line.strip())
            tF.readline().strip() # second line is a comment that we throw away
            q = []
            for i in range(N):
                line = tF.readline().strip().split(" ")
                for c in line[1:]:
                    if c is not "":
                        q.append(float(c))
            trajectory.append(np.array(q))

            line = tF.readline()

    return trajectory, N



def writeTrajectory(trajFile,q,atomicity,timeStep,stop,molMap,file):
    if timeStep == 0 :
        file = open(trajFile, 'w')
    
    file.write('%s\n' % (np.shape(q)[0]))
    file.write('Time step: %s\n' % (timeStep))
    for i in range(np.shape(q)[0]):
        file.write('%s %8s %8s %8s\n' % (molMap[i%atomicity],q[i,0],q[i,1],q[i,2]))
        
    if timeStep == stop-1 :
        file.close()
        
    return file

def writeTrajectoryMix(trajFile,q,timeStep,stop,molMap,file):
	if timeStep == 0 :
		file = open(trajFile, 'w')

	file.write('%s\n' % (np.shape(q)[0]))
	file.write('Time step: %s\n' % (timeStep))
	for i in range(np.shape(q)[0]):
		print(i)
		file.write('%s %8s %8s %8s\n' % (molMap[i],q[i,0],q[i,1],q[i,2]))
        
	if timeStep == stop-1 :
		file.close()
        
	return file
        
# =============================================================================
# 	with open(trajFile,"a+") as tf:
# 		tf.write(str(q.shape[0]) + '\n')
# 		tf.write('Time step:' + str(timeStep) + '\n')
# 		#print(str(atomicity*q.shape[0]) + '\n')
# 		#print('Time step:' + str(timeStep) + '\n')
# 		it = 0
# 		for i in range(int(q.shape[0]/atomicity)):
# 			for j in range(atomicity):
# 				tf.write(molMap[j]+'\t \t'+str(float(q[it][0]))+'\t \t'+str(float(q[it][1]))+'\t \t'+str(float(q[it][2]))+'\n')
# 				#print(molMap[j]+'\t \t'+str(q[i+j][0])+'\t \t'+str(q[i+j][1])+'\t \t'+str(q[i+j][2])+'\n')
# 				it = it+1
# =============================================================================

def writeMoleculePositions(posFile,q,molMap):
    with open(posFile,'a+') as tf:
        for i in range(q.shape[0]):
            tf.write(molMap[i]+'\t \t'+str(float(q[i][0]))+'\t \t'+str(float(q[i][1]))+'\t \t'+str(float(q[i][2]))+'\n')
          
            
def initializePositions(q,L,molType,molDensity,nDimensions):

	atomicity = q.shape[0]
	distanceFactor = 1/molDensity
	if (molType == 'Water'):
		nMols = int(molDensity*L//(np.linalg.norm(q[1]-q[2])))	#q1 and q2 are positions of the hydrogens which are the farthest 
		offset = distanceFactor*np.linalg.norm(q[1]-q[2])
	else:
		nMols = int(molDensity*L//(np.linalg.norm(q[4]-q[8])))
		offset = distanceFactor*np.linalg.norm(q[4]-q[8])	#q4 and q8 are the positions of atoms that are the farthest in the ethanol mol.
	it = 0
	if (nDimensions == 2):
		qn = np.zeros((nMols*(nMols)*atomicity,3))
		for i in range(0,nMols):
			for j in range(0,nMols):
				qn[it:it+atomicity] = q + np.array([i*offset,j*offset,0])
				it = it+atomicity
	elif (nDimensions == 1):
		qn = np.zeros(((nMols)*atomicity,3))
		for i in range(0,nMols):
			qn[it:it+atomicity] = q + np.array([i*offset,0,0])
			it = it+atomicity
	else:
		qn = np.zeros((nMols*nMols*nMols*atomicity,3))
		for i in range(0,nMols):
			for j in range(0,nMols):
				for k in range(0,nMols):
					qn[it:it+atomicity] = q + np.array([i*offset,j*offset,k*offset])
					it = it+atomicity
	
	
	return qn          
	
def initializeMixture(qWater, qEthanol, nMols, L, nDimensions):
	nEmols = int(np.ceil(0.135*nMols))
	nWmols = nMols - nEmols
	eAtomicity = qEthanol.shape[0]
	wAtomicity = qWater.shape[0] 
	cell = np.linalg.norm(qEthanol[4] - qEthanol[8])
	itE = 0
	itW = 0
	if (nDimensions == 2):
		nEmols = int(np.sqrt(nEmols))
		nWmols = int(np.sqrt(nWmols))
		qnE = np.zeros((nEmols*(nEmols)*eAtomicity,3))
		qnW = np.zeros((nWmols*(nWmols)*wAtomicity,3))
		offsetE = cell*(np.floor(L/cell)/nEmols)
		offsetW = cell*(np.floor(L/cell)/nWmols)
		#Ethanol lattice
		for i in range(0,nEmols):
			for j in range(0,nEmols):
				qnE[itE:itE+eAtomicity] = qEthanol + np.array([i*offsetE,j*offsetE,0])
				itE = itE+eAtomicity
		#Water Lattice
		for i in range(0,nWmols):
			for j in range(0,nWmols):
				qnW[itW:itW+wAtomicity] = qWater + np.array([i*offsetW,j*offsetW,0])
				itW = itW+wAtomicity
		nEmols = nEmols**2
		nWmols = nWmols**2
	elif (nDimensions == 1):
		qnE = np.zeros(((nEmols)*eAtomicity,3))
		qnW = np.zeros(((nWmols)*wAtomicity,3))
		offsetE = cell*(np.floor(L/cell)/nEmols)
		offsetW = cell*(np.floor(L/cell)/nWmols)
		#Ethanol lattice
		for i in range(0,nEmols):
			qnE[itE:itE+eAtomicity] = qEthanol + np.array([i*offsetE,0,0])
			itE = itE+eAtomicity
		#Water lattice
		for i in range(0,nWmols):
			qnW[itW:itW+wAtomicity] = qWater + np.array([i*offsetW,0,0])
			itW = itW+wAtomicity
	else:
		nEmols = int(np.cbrt(nEmols))
		nWmols = int(np.cbrt(nWmols))
		qnE = np.zeros((nEmols*nEmols*nEmols*eAtomicity,3))
		qnW = np.zeros((nWmols*nWmols*nWmols*wAtomicity,3))
		offsetE = cell*(np.floor(L/cell)/nEmols)
		offsetW = cell*(np.floor(L/cell)/nWmols)
		#Ethanol lattice
		for i in range(0,nEmols):
			for j in range(0,nEmols):
				for k in range(0,nEmols):
					qnE[itE:itE+eAtomicity] = qEthanol + np.array([i*offsetE,j*offsetE,k*offsetE])
					itE = itE+eAtomicity
		#Water Lattice
		for i in range(0,nWmols):
			for j in range(0,nWmols):
				for k in range(0,nWmols):
					qnW[itW:itW+wAtomicity] = qWater + np.array([i*offsetW,j*offsetW,k*offsetW])
					itW = itW+wAtomicity
		nEmols = nEmols**3
		nWmols = nWmols**3	
	return [np.append(qnE,qnW,axis=0),nEmols,nWmols] 
