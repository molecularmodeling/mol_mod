import numpy as np
import matplotlib.pyplot as plt
import trajectoryIO
import integrationSchemes
	
#---------------------------Main Script----------------#
eqbmLength = 0.9572
kL = 5
tFinal = 25
delT = 0.01
atomicityHydrogen = 2
eps = 0.66386
sigma = 0.315061
mHydrogen = [1,1]
bondsWater = ["H","H"]


t,nAtoms = trajectoryIO.readTrajectory('Hydrogent0.xyz')
q = t[0].reshape(nAtoms,3)



#Initial  conditions

v = np.zeros((nAtoms,3))
timeSteps = int((tFinal-0.0)/delT)
file = 0
#Integration
for i in range(timeSteps):
	print("Time step: ",i,"\n")
	file = trajectoryIO.writeTrajectory('wrHydrogen.xyz',q,atomicityHydrogen,i*delT,timeSteps,bondsWater,file)
	[q,v] = integrationSchemes.velVerletIntHydrogen(q,v,atomicityHydrogen,mHydrogen,eqbmLength,kL,eps,sigma,tFinal,delT)

#-------------------End of main script-------------#
