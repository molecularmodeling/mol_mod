# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 15:56:12 2018

@author: hemaditya
"""

import numpy as np
import matplotlib.pyplot as plt
import trajectoryIO




#---------------Water---------------#
eqbmLength = 0.9572
eqbmAngle = (np.pi*104.52)/180
qW = np.array([[2, 1, 1],
     [2+eqbmLength, 1, 1],
     [2+eqbmLength*np.cos(eqbmAngle), 1+eqbmLength*np.sin(eqbmAngle), 1]])
    
    
#--------------------Ethanol---------------#

qE = np.array([[ 2.768421,  3.149159,  2.962525],
       [ 3.229441,  3.87316 ,  4.22485 ],
       [ 3.868228,  2.948372,  5.114423],
       [ 3.619613,  2.666246,  2.43429 ],
       [ 2.290555,  3.852087,  2.245393],
       [ 2.023607,  2.355802,  3.191635],
       [ 2.371215,  4.360022,  4.73635 ],
       [ 3.952253,  4.674538,  3.962081],
       [ 3.204846,  2.380437,  5.483509]])
#---------------------------------------------------------------#
L = 50
qC = np.append(qE,qW,axis = 0)
N = 500
nDim = 3

[q,ne,nw] = trajectoryIO.initializeMixture(qW,qE,N,L,nDim)
l = (9*ne+3*nw)*["F"]

file = 0
file = trajectoryIO.writeTrajectoryMix('mixtureTest.xyz',q,0,1,["C","C","O","H","H","H","H","H","H"]*ne+["O","H","H"]*nw,file)
#file = trajectoryIO.writeTrajectoryMix('mixtureTest.xyz',q,0,1,l,file)
       
       

