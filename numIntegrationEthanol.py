import numpy as np
import matplotlib.pyplot as plt
import trajectoryIO
import integrationSchemes
import forces
	
#---------------------------Main Script----------------#
 
#This connectivity matrix is based on the initial ordering of the initial conditions file.
connectivityMatrix = [[1,3,4,5],[0,2,6,7],[1,8],[0],[0],[0],[1],[1],[2]]

#Eqbm length combinations- based on the connectivity matrix
eqbmLength = [10*np.array([0.1529,0.1090,0.1090,0.1090]),10*np.array([0.1529,0.1410,0.1090,0.1090]),10*np.array([0.1410,0.0945]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.0945])]
#eqbmLength = [np.array([1.5265,1.1121,1.1121,1.1121]),np.array([1.5265,1.4334,1.1121,1.1121]),np.array([1.4334,0.9481]),np.array([1.1121]),np.array([1.1121]),np.array([1.1121]),np.array([1.1121]),np.array([1.1121]),np.array([0.9481])]
kL = [0.01*np.array([224262.4,284512.0,284512.0,284512.0]),0.01*np.array([224262.4,267776.0,284512.0,284512.0]),0.01*np.array([267776.0,462750.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([462750.0])]
aclist = [[0,1],[0,2],[0,3],[0,0],[1,2],[1,3],[1,1],[2,1]]#This is not the arc list. This is a matrix that stores the indices of the atoms in the connectivity matrix
#Eqbm angle- based on the connectivity matrix- element- pair-wise combinations
eqbmAngle = [[108.5,108.5,108.5,107.8,107.8,107.8],[109.5,110.7,110.7,109.5,109.5,107.8],[108.5]]
kT = [0.01*np.array([292.88,292.88,292.88,276.144,276.144,276.144]),0.01*np.array([414.4,313.8,313.8,292.88,292.88,276.144]),0.01*np.array([460.240])]
#Dihedral Data- yet to change to reduced units
dihedralConMat = [['3 0 1 6','4 0 1 6','5 0 1 6','3 0 1 7','4 0 1 7','5 0 1 7'],['3 0 1 2','4 0 1 2','5 0 1 2'],['0 1 2 8'],['6 1 2 8','7 1 2 8']]
dihedralData = [[0.62760,1.88280,0.0,-3.91622],[0.97905,2.93716,0.0,-3.91622],[-0.4431,3.8255,0.72801,-4.11705],[0.94140,2.82420,0.0,-3.76560]]
#Lennard Jones Potential Data
eps = np.array([0.35,0.35,0.312,0.25,0.25,0.25,0.25,0.25,0.0])
sigma = np.array([2.76144,2.76144,7.11280,1.25520,1.25520,1.25520,1.25520,1.25520,0.00])

tFinal = 250 #1000 is the given nonD time
delT = 0.02 #0.02 is the given nonD delt
atomicityEthanol = 9
mEthanol = np.array([12,12,16,1,1,1,1,1,1])

"""
#Test unit data-------------------------
eqbmLength = [[1.5,1,1,1],[1,1,1,1],[1,1],[1],[1],[1],[1],[1],[1]]
kL = [[10,1,1,1],[1,1,1,1],[1,1],[1],[1],[1],[1],[1],[1]]
#Eqbm angle- based on the connectivity matrix- element- pair-wise combinations
#Try1
eqbmAngle = [[108.5,108.5,108.5,107.8,107.8,107.8],[109.5,110.7,110.7,109.5,109.5,107.8],[108.5]]
kT = [[1,1,1,1,1,1],[1,1,1,1,1,1],[1]]
#Try2
angData = [[1,0,3],[1,0,4],[1,0,5],[4,0,5],[3,0,5],[4,0,3],[6,1,7],[0,1,7],[0,1,6],[0,1,2],[1,2,8],[6,1,2],[7,1,2]]
eqbmAngle = [108.5,108.5,108.5,107.8,107.8,107.8,107.8,110.7,109.5,108.5,109.5]
kT = [292.88,292.88,292.88,292.88,#incomplete]
#Dihedral Data
dihedralConMat = [['3 0 1 6','4 0 1 6','5 0 1 6','3 0 1 7','4 0 1 7','5 0 1 7'],['3 0 1 2','4 0 1 2','5 0 1 2'],['0 1 2 8'],['6 1 2 8','7 1 2 8']]
dihedralData = [[1,1,0,1],[1,1,0.0,1],[1,1,1,1],[1,1,0.0,1]]
#Lennard Jones Potential Data
eps = [0.35,0.35,0.312,0.25,0.25,0.25,0.25,0.25,0.0]
sigma = [0.276144,0.276144,0.711280,0.125520,0.125520,0.125520,0.125520,0.125520,0.00]

tFinal = 50
delT = 0.001
atomicityEthanol = 9
mEthanol = np.array([12,12,16,1,1,1,1,1,1])"""
#End Test data------------------------------



bondsEthanol = ["C","C","O","H","H","H","H","H","H"]

t,nAtoms = trajectoryIO.readTrajectory('Ethanolt0.xyz')
qInit = t[0].reshape(nAtoms,3)
q = trajectoryIO.initializePositions(qInit,50,'Ethanol',0.2,3)
nAtoms = q.shape[0]

#Testing for total energy
#eqbmLength = [np.array([np.linalg.norm(q[0] - q[1]),np.linalg.norm(q[0] - q[3]),np.linalg.norm(q[0] - q[4]),np.linalg.norm(q[0] - q[5])]),np.array([np.linalg.norm(q[1] - q[0]),np.linalg.norm(q[1] - q[2]),np.linalg.norm(q[1] - q[6]),np.linalg.norm(q[1] - q[7])]),np.array([np.linalg.norm(q[2] - q[1]),np.linalg.norm(q[2] - q[8])]),np.array([np.linalg.norm(q[3] - q[0])]),np.array([np.linalg.norm(q[4] - q[0])]),np.array([np.linalg.norm(q[5] - q[0])]),np.array([np.linalg.norm(q[6] - q[1])]),np.array([np.linalg.norm(q[7] - q[1])]),np.array([np.linalg.norm(q[8] - q[2])])]

#Initial  conditions
print('Start simulation!')
v = np.zeros((nAtoms,3))
timeSteps = int((tFinal-0.0)/delT)
k = np.zeros(timeSteps)
rcom = np.zeros((timeSteps,3))
L =50
qdiff = np.zeros(timeSteps)
potdiff = np.zeros(timeSteps)
P = 0
p = np.zeros(timeSteps)
Elj = np.zeros(timeSteps)
file = 0
f = 0
#Integration
for i in range(timeSteps):
	print("Time step: ",i,"\n")
	vMol = forces.vCom(v,mEthanol)
	#print('Velocity of COM: ',vMol)
	k[i] = sum(forces.kineticEnergy(v,mEthanol))
	#print('Average KE of the molecule:', k[i])
	#rcom[i] = forces.rCom(q,mEthanol)
	#print('rCom of the molecule:', rcom[i])
	#L[i] = forces.angMom(q,v,mEthanol)
	#print("Net angular momentum of the molecule:", L[i])
	qTest = q
	pTest = P
	file = trajectoryIO.writeTrajectory('wrEthanol.xyz',q,atomicityEthanol,i*delT,timeSteps,bondsEthanol,file)
	for j in range(0,nAtoms,atomicityEthanol):
		for ii in range(j+atomicityEthanol,nAtoms,atomicityEthanol):
			dqTemp = q[ii:ii+atomicityEthanol] - q[j:j+atomicityEthanol,np.newaxis]
			dq = np.reshape(dqTemp,[dqTemp.shape[0]*dqTemp.shape[1],3])
			sigmaTemp = (sigma + sigma[:,np.newaxis])/2
			sigmaMix = np.reshape(sigmaTemp,sigmaTemp.shape[0]*sigmaTemp.shape[1])
			epsTemp = np.sqrt(eps * eps[:,np.newaxis])
			epsMix = np.reshape(epsTemp,epsTemp.shape[0]*epsTemp.shape[1])
			
			dist = (dq) - L*np.floor(dq/L+0.5)
			temporary = (np.divide(sigmaMix,np.linalg.norm(dist,axis=1)))**6
			Elj[i] = sum(4*epsMix*(temporary**2-temporary))
	[q,v,f,P] = integrationSchemes.velVerletIntEthanol(q,v,atomicityEthanol,mEthanol,connectivityMatrix,eqbmLength,kL,aclist,eqbmAngle,kT,eps,sigma,dihedralConMat,dihedralData,f,P,i,delT,50)
	qdiff[i] = sum(np.linalg.norm(q - qTest,axis=1))
	potdiff[i] = sum(P - pTest)
	p[i] = sum(P)
"""plt.figure(1)
plt.plot(np.linspace(0,tFinal,timeSteps),L)
plt.show()"""
plt.figure(1)
plt.plot(np.linspace(0,tFinal,timeSteps)*1e-4,k)
plt.xlabel('time(ns)')
plt.ylabel('Kinetic Energy(kJ/mol)')
plt.show()
plt.figure(2)
plt.plot(np.linspace(0,tFinal,timeSteps)*1e-4,p)
plt.xlabel('time(ns)')
plt.ylabel('Potential Energy(kJ/mol)')
plt.show()
plt.figure(3)
plt.plot(np.linspace(0,tFinal,timeSteps)*1e-4,(p+k+Elj))
plt.xlabel('time(ns)')
plt.ylabel('Total Energy(kJ/mol)')
plt.show()
plt.plot(4)
plt.plot(np.linspace(0,tFinal,timeSteps)*1e-4,qdiff)
plt.xlabel('time(ns)')
plt.ylabel('Position difference')
plt.show()
plt.plot(5)
plt.plot(np.linspace(0,tFinal,timeSteps)*1e-4,potdiff)
plt.xlabel('time(ns)')
plt.ylabel('Potential difference')
plt.show()
plt.plot(6)
t = p+k
plt.plot(np.linspace(0,tFinal,timeSteps)*1e-4,Elj)
plt.xlabel('time(ns)')
plt.ylabel('Lennard Jones')
plt.show()

#-------------------End of main script-------------#
