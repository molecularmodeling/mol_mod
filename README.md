Molecular Modelling Project

This repository contains the work done create MD simulations of water, ethanol mixtures. All of the code has been written in python. 
The final set of commented codes and the necessary modules are in the folder "Final_Codes". 
This was a project done as a part of the graduate course "Introduction to Molecular Modelling".

For further queries and/or suggestions, please feel free to contact:
1) Teun van Roosmalen- a.h.v.roosmalen@student.tue.nl
2) Ellen Vugts- e.m.t.vugts@student.tue.nl
3) Hemaditya Malla- h.malla@student.tue.nl


%%Commons Licence here%%%