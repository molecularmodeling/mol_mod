import numpy as np
import matplotlib.pyplot as plt
import trajectoryIO
import integrationSchemes
import forces
import time
	
#---------------------------Main Script----------------#
eAngleW = (np.pi*104.52)/180
eLengthW = 0.9572
kLW = 5024.16
kTW = 6.2802

q_initW = np.array([[2, 1, 0],
    [2+eLengthW, 1, 0],
    [2+eLengthW*np.cos(eAngleW), 1+eLengthW*np.sin(eAngleW), 0]])
q_initE,unused = trajectoryIO.readTrajectory('Ethanolt0.xyz')
q_initE = np.reshape(q_initE, (unused,3))+np.array([6,2,1])


N = 100
L = 30

[q,eMols,wMols] = trajectoryIO.initializeMixture(q_initW,q_initE,N,L,3)


#This connectivity matrix is based on the initial ordering of the initial conditions file.
connectivityMatrix = [[1,3,4,5],[0,2,6,7],[1,8],[0],[0],[0],[1],[1],[2]]

#Eqbm length combinations- based on the connectivity matrix
eqbmLength = [10*np.array([0.1529,0.1090,0.1090,0.1090]),10*np.array([0.1529,0.1410,0.1090,0.1090]),10*np.array([0.1410,0.0945]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.1090]),10*np.array([0.0945])]
#eqbmLength = [np.array([1.5265,1.1121,1.1121,1.1121]),np.array([1.5265,1.4334,1.1121,1.1121]),np.array([1.4334,0.9481]),np.array([1.1121]),np.array([1.1121]),np.array([1.1121]),np.array([1.1121]),np.array([1.1121]),np.array([0.9481])]
kL = [0.01*np.array([224262.4,284512.0,284512.0,284512.0]),0.01*np.array([224262.4,267776.0,284512.0,284512.0]),0.01*np.array([267776.0,462750.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([284512.0]),0.01*np.array([462750.0])]
aclist = [[0,1],[0,2],[0,3],[0,0],[1,2],[1,3],[1,1],[2,1]]#This is not the arc list. This is a matrix that stores the indices of the atoms in the connectivity matrix
#Eqbm angle- based on the connectivity matrix- element- pair-wise combinations
eqbmAngle = [[108.5,108.5,108.5,107.8,107.8,107.8],[109.5,110.7,110.7,109.5,109.5,107.8],[108.5]]
kT = [0.01*np.array([292.88,292.88,292.88,276.144,276.144,276.144]),0.01*np.array([414.4,313.8,313.8,292.88,292.88,276.144]),0.01*np.array([460.240])]
#Dihedral Data- yet to change to reduced units
dihedralConMat = [['3 0 1 6','4 0 1 6','5 0 1 6','3 0 1 7','4 0 1 7','5 0 1 7'],['3 0 1 2','4 0 1 2','5 0 1 2'],['0 1 2 8'],['6 1 2 8','7 1 2 8']]
dihedralData = [[0.62760,1.88280,0.0,-3.91622],[0.97905,2.93716,0.0,-3.91622],[-0.4431,3.8255,0.72801,-4.11705],[0.94140,2.82420,0.0,-3.76560]]
#Lennard Jones Potential Data
epsE = np.array([0.276144,0.276144,0.711280,0.125520,0.125520,0.125520,0.125520,0.125520,0.00])
epsW = np.array([0.66386,0.0,0.0])
eps = np.hstack((np.tile(epsE,eMols),np.tile(epsW,wMols)))
sigmaE = np.array([0.35,0.35,0.312,0.25,0.25,0.25,0.25,0.25,0.0])
sigmaW = sigma = np.array([3.15061,0.0,0.0])
sigma = np.hstack((np.tile(sigmaE,eMols),np.tile(sigmaW,wMols)))


tFinal = 1000 #1000 is the given nonD time
delT = 0.02 #0.02 is the given nonD delt

mEthanol = np.array([12,12,16,1,1,1,1,1,1])
mWater = np.array([16,1,1])
m = np.hstack((np.tile(mEthanol,eMols),np.tile(mWater,wMols)))

bondsEthanol = ["C","C","O","H","H","H","H","H","H"]
bondsWater = ["O","H","H"]
bonds = []
for i in range(eMols):
	bonds.extend(bondsEthanol)
for i in range(wMols):
	bonds.extend(bondsWater)



#Initial  conditions
print('Start simulation!')
k = 0.166
T = 250
T = T/120.27
mean = 0
sigma_speed = np.sqrt((k*T)/m)
v = np.vstack((np.vstack((np.random.normal(mean,sigma_speed),np.random.normal(mean,sigma_speed))),np.random.normal(mean,sigma_speed))).T
timeSteps = int((tFinal-0.0)/delT)
Ek = np.zeros(timeSteps)
Eb = np.zeros(timeSteps)
Ea = np.zeros(timeSteps)
Elj = np.zeros(timeSteps)
temp = np.zeros(timeSteps)
file = 0
f = 0

start_timing = time.time()
#Integration
for i in range(timeSteps):
	if (i%1000==0):
		print("Time step: ",i,"\n")

	file = trajectoryIO.writeTrajectoryMix('wrMix250.xyz',q,i*delT,timeSteps,bonds,file)
    
# =============================================================================
# 	for j in range(0,nAtoms,atomicityEthanol):
# 		for ii in range(j+atomicityEthanol,nAtoms,atomicityEthanol):
# 			dqTemp = q[ii:ii+atomicityEthanol] - q[j:j+atomicityEthanol,np.newaxis]
# 			dq = np.reshape(dqTemp,[dqTemp.shape[0]*dqTemp.shape[1],3])
# 			sigmaTemp = (sigma + sigma[:,np.newaxis])/2
# 			sigmaMix = np.reshape(sigmaTemp,sigmaTemp.shape[0]*sigmaTemp.shape[1])
# 			epsTemp = np.sqrt(eps * eps[:,np.newaxis])
# 			epsMix = np.reshape(epsTemp,epsTemp.shape[0]*epsTemp.shape[1])
# 			
# 			dist = (dq) - L*np.floor(dq/L+0.5)
# 			temporary = (np.divide(sigmaMix,np.linalg.norm(dist,axis=1)))**6
# 			Elj[i] = sum(4*epsMix*(temporary**2-temporary))
# =============================================================================
	Tnew = forces.temperature(v,k,m)
	v = np.sqrt(T/Tnew)*v
	temp[i] = Tnew*120.27
	[q,v,f] = integrationSchemes.velVerletIntMix(q,v,eMols,wMols,m,connectivityMatrix,eqbmLength,kL,eqbmAngle,kT,eps,sigma,f,i,delT,L)

runTime = time.time() - start_timing

plt.plot(1e-4*np.linspace(0,timeSteps*delT,timeSteps),temp)
plt.xlabel("Time (ns)")
plt.ylabel("Temperature (K)")
plt.show
#-------------------End of main script-------------#


