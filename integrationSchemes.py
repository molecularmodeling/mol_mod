import numpy as np
import forces
import itertools
		
def velVerletIntWater(q,v,atomicity,m,kT,eqbmLength,kL,eqbmAngle,eps,sigma,tFinal,delT,L,F,time,tStart = 0):
		nMols = int(q.shape[0]/atomicity)
		qn = np.zeros((q.shape[0],3))
		vn = np.zeros((v.shape[0],3))
		
		if (time == tStart):
			fthalf = np.zeros((q.shape[0],3))
			iMol = 0
			
			#Looping through each molecule
			for i in range(0,q.shape[0],atomicity):
			#Angular Force Computation
				AngF = forces.bondedAngularForce(q[i:i+3:1],kT,eqbmAngle)
			#Bonded Force Computation
				f = np.zeros((atomicity,3))
				f[1] = forces.bforce(forces.bondVec(q[i],q[i+1]),eqbmLength,kL) + AngF[1]
				f[0] = forces.bforce(forces.bondVec(q[i+1],q[i]),eqbmLength,kL) + forces.bforce(forces.bondVec(q[i+2],q[i]),eqbmLength,kL) + AngF[0]
				f[2] = forces.bforce(forces.bondVec(q[i],q[i+2]),eqbmLength,kL) + AngF[2]

				fthalf[i:i+atomicity] = f
				iMol = iMol+1
			fthalf = fthalf + forces.forceLJ(q,atomicity,sigma,eps,L)
		else:
			fthalf = F
		
		qn = q + delT*v + delT**2/(2*np.tile(m[:,np.newaxis],(nMols,1)))*fthalf
		#Periodic Boundary conditions
		qCom = forces.rCom(qn,m)
		atomic = atomicity * np.ones(nMols).astype(int)	#This will not be necessary once atomicity becomes a vector
		qCom = np.repeat(qCom,atomic,axis=0)
		qn = qn - qCom + (qCom%L) #Might look strange, I will explain this in person

		
		
		
		iMol = 0
		ftotal = np.zeros((q.shape[0],3))
		for i in range(0,qn.shape[0],atomicity):
		#Angular Force Computation- new timeStep
			AngF = forces.bondedAngularForce(qn[i:i+3:1],kT,eqbmAngle)
		#Bonded Force Computation- new timeStep
			ft = np.zeros((atomicity,3))
			ft[1] = forces.bforce(forces.bondVec(qn[i],qn[i+1]),eqbmLength,kL) + AngF[1]
			ft[0] = forces.bforce(forces.bondVec(qn[i+1],qn[i]),eqbmLength,kL) + forces.bforce(forces.bondVec(qn[i+2],qn[i]),eqbmLength,kL) + AngF[0]
			ft[2] = forces.bforce(forces.bondVec(qn[i],qn[i+2]),eqbmLength,kL) + AngF[2]
			ftotal[i:i+atomicity] = ft

			iMol = iMol+1 
		#Integration- velocities
		ftotal = ftotal + forces.forceLJ(qn,atomicity,sigma,eps,L)
		vn = v+ (delT/(2*np.tile(m[:,np.newaxis],(nMols,1)))*(fthalf + ftotal))

		return [qn,vn,ftotal]
		
		
def velVerletIntEthanol(q,v,atomicity,m,conMat,eqbmLength,kL,aclist,eqbmAngle,kT,eps,sigma,dcm,dd,F,P,time,delT,L,tStart = 0):
	nMols = int(q.shape[0]/atomicity)
	qn = np.zeros((q.shape[0],3))	#New position vectors
	vn = np.zeros((q.shape[0],3))	#New velocity vectors
	#Looping through each molecule
	if (time == tStart):
		fthalf = np.zeros((q.shape[0],3))
		potE = np.zeros(nMols)
		potIter = 0
		for i in range(0,q.shape[0],atomicity):
			#print('Molecule:',i)
			f = np.zeros((atomicity,3))	#Initialize the force vector for each atom of a molecule
			pe = 0	#Initialize the bonded potential vector for each atom of a molecule
			#Separating the position and velocity vectors(easier to do other operations)
			qTemp = q[i:i+atomicity]	
			vTemp = v[i:i+atomicity]
			#Bonded potentials
			potE[potIter] = potE[potIter] + forces.bondedPotentialEthanol(qTemp,aclist,eqbmLength,kL) 
			#Computing the positions and forces for each atom at t+dt/2
			for j in range(atomicity):
				#print('Atom number:',j)
            	#Bonded Forces/potentials
				f[j] = f[j] + sum(forces.bforceEth(forces.bondVec(qTemp[j],qTemp[conMat[j]]),np.array(eqbmLength[j]),np.array(kL[j])))
        		#Angular forces/potentials- only looping through entries with at least two atoms
				if (len(conMat[j]) >= 2):
					#Obtaining the the combinations for the forces
					combos = list(itertools.combinations(conMat[j],2))
					for k in range(len(combos)):
                	    #Order of atoms for the angular force computation= a1-aj-a2
						[ftj,ft0,ft1] = forces.bondedAngularForceEth(qTemp[j],qTemp[combos[k][0]],qTemp[combos[k][1]],eqbmAngle[j][k],np.array(kT[j][k]))
						f[j] = f[j] + ftj
						f[combos[k][0]] = f[combos[k][0]] + ft0
						f[combos[k][1]] = f[combos[k][1]] + ft1
						potE[potIter] = potE[potIter] + forces.bondedAngularPotentialEthanol(qTemp[j],qTemp[combos[k][0]],qTemp[combos[k][1]],eqbmAngle[j][k],np.array(kT[j][k]))
			#Computing the non-bonded forces
				#f[j] = f[j] + forces.nbforce(forces.nbDistance(q,j,int(i//atomicity),atomicity),sigma[j],eps[j])
        	#Torsional Forces
			"""for l in range(len(dcm)):
				for ii in range(len(dcm[l])):
					temp = dcm[l][ii].split()
					fDihedral = np.zeros((len(temp),3))
					fDihedral = forces.tforce(qTemp[int(temp[0])],qTemp[int(temp[1])],qTemp[int(temp[2])],qTemp[int(temp[3])],dd[l],L) 
					f[int(temp[0])] = f[int(temp[0])] + fDihedral[0]
					f[int(temp[1])] = f[int(temp[1])] + fDihedral[1]
					f[int(temp[2])] = f[int(temp[2])] + fDihedral[2]
					f[int(temp[3])] = f[int(temp[3])] + fDihedral[3]
					potE[potIter] = potE[potIter] + forces.tpotential(qTemp[int(temp[0])],qTemp[int(temp[1])],qTemp[int(temp[2])],qTemp[int(temp[3])],dd[l])"""
			fthalf[i:i+atomicity] = f
			potIter = potIter + 1
		fthalf = fthalf + forces.forceLJ(q,atomicity,sigma,eps,L)
	else:
		fthalf = F
		potE = P 				       
    
	#Integration position
	qn = q + delT*v + delT**2/(2*np.tile(m[:,np.newaxis],(nMols,1)))*fthalf#--------> This command is amazing
	#Periodic Boundary conditions
	qCom = forces.rCom(qn,m)
	atomic = atomicity * np.ones(nMols).astype(int)	#This will not be necessary once atomicity becomes a vector
	qCom = np.repeat(qCom,atomic,axis=0)
	qn = qn - qCom + (qCom%L) #Might look strange, I will explain this in person
          
	ftotal = np.zeros((q.shape[0],3))
	potE = np.zeros(nMols)
	potIter = 0      
	for i in range(0,q.shape[0],atomicity):        
        #New forces
		fn = np.zeros((atomicity,3))
		qTemp = qn[i:i+atomicity]
		#Bonded potentials
		potE[potIter] = potE[potIter] + forces.bondedPotentialEthanol(qTemp,aclist,eqbmLength,kL)
		for j in range(atomicity):
            #Bonded Forces
			fn[j] = fn[j] + sum(forces.bforceEth(forces.bondVec(qTemp[j],qTemp[conMat[j]]),np.array(eqbmLength[j]),np.array(kL[j])))
        	#Angular forces- only looping through entries with at least two atoms
			if (len(conMat[j]) >= 2):
				#Obtaining the the combinations for the forces
				combos = list(itertools.combinations(conMat[j],2))
				for k in range(len(combos)):
                    #Order of atoms for the angular force computation= a1-aj-a2
					[ftj,ft0,ft1] = forces.bondedAngularForceEth(qTemp[j],qTemp[combos[k][0]],qTemp[combos[k][1]],eqbmAngle[j][k],np.array(kT[j][k]))
					fn[j] = fn[j] + ftj
					fn[combos[k][0]] = fn[combos[k][0]] + ft0
					fn[combos[k][1]] = fn[combos[k][1]] + ft1
					potE[potIter] = potE[potIter] + forces.bondedAngularPotentialEthanol(qTemp[j],qTemp[combos[k][0]],qTemp[combos[k][1]],eqbmAngle[j][k],np.array(kT[j][k]))

        #Torsional Forces
		"""for l in range(len(dcm)):
			for ii in range(len(dcm[l])):
				temp = dcm[l][ii].split()
				fDihedral = forces.tforce(qTemp[int(temp[0])],qTemp[int(temp[1])],qTemp[int(temp[2])],qTemp[int(temp[3])],dd[l],L) 
				fn[int(temp[0])] = fn[int(temp[0])] + fDihedral[0]
				fn[int(temp[1])] = fn[int(temp[1])] + fDihedral[1]
				fn[int(temp[2])] = fn[int(temp[2])] + fDihedral[2]
				fn[int(temp[3])] = fn[int(temp[3])] + fDihedral[3]
				potE[potIter] = potE[potIter] + forces.tpotential(qTemp[int(temp[0])],qTemp[int(temp[1])],qTemp[int(temp[2])],qTemp[int(temp[3])],dd[l])"""
		ftotal[i:i+atomicity] = fn
		potIter = potIter + 1
	ftotal = ftotal + forces.forceLJ(qn,atomicity,sigma,eps,L)
	#Integrating velocity
	vn = v+ (delT/(2*np.tile(m[:,np.newaxis],(nMols,1)))*(fthalf + ftotal)) #--------> This command is amazing!
	return [qn,vn,ftotal,potE]


def velVerletIntMix(q,v,eMols,wMols,m,conMat,eqbmLength,kL,eqbmAngle,kT,eps,sigma,F,time,delT,L,tStart = 0):
	eAngleW = (np.pi*104.52)/180
	eLengthW = 0.9572
	kLW = 5024.16
	kTW = 6.2802
	
	atomicity = np.repeat([9,3],[9*eMols,3*wMols])
	atomE = atomicity[0]
	atomW = atomicity[9*eMols]
	qn = np.zeros((q.shape[0],3))
	vn = np.zeros((v.shape[0],3))
		
	if (time == tStart):
		fthalf = np.zeros((q.shape[0],3))
		iMol = 0
		
		#Looping through each ethanol molecule
		for i in range(0,eMols*atomE,atomE):
			f=np.zeros((atomE,3))	#Initialize the force vector for each atom of a molecule
			#Separating the position and velocity vectors(easier to do other operations)
			qTemp = q[i:i+atomE]	
			vTemp = v[i:i+atomE]
			#Computing the positions and forces for each atom at t+dt/2
			for j in range(atomE):
				#Bonded Forces/potentials
				f[j] = f[j] + sum(forces.bforceEth(forces.bondVec(qTemp[j],qTemp[conMat[j]]),np.array(eqbmLength[j]),np.array(kL[j])))
				#Angular forces/potentials- only looping through entries with at least two atoms
				if (len(conMat[j]) >= 2):
					#Obtaining the the combinations for the forces
					combos = list(itertools.combinations(conMat[j],2))
					for k in range(len(combos)):
						#Order of atoms for the angular force computation= a1-aj-a2
						[ftj,ft0,ft1] = forces.bondedAngularForceEth(qTemp[j],qTemp[combos[k][0]],qTemp[combos[k][1]],eqbmAngle[j][k],np.array(kT[j][k]))
						f[j] = f[j] + ftj
						f[combos[k][0]] = f[combos[k][0]] + ft0
						f[combos[k][1]] = f[combos[k][1]] + ft1

			fthalf[i:i+atomE] = f
			
			
		for i in range(eMols*atomE,q.shape[0],atomW):
			
			#Angular Force Computation
			AngF = forces.bondedAngularForce(q[i:i+3:1],kTW,eAngleW)
			#Bonded Force Computation
			f = np.zeros((atomW,3))
			f[1] = forces.bforce(forces.bondVec(q[i],q[i+1]),eLengthW,kLW) + AngF[1]
			f[0] = forces.bforce(forces.bondVec(q[i+1],q[i]),eLengthW,kLW) + forces.bforce(forces.bondVec(q[i+2],q[i]),eLengthW,kLW) + AngF[0]
			f[2] = forces.bforce(forces.bondVec(q[i],q[i+2]),eLengthW,kLW) + AngF[2]

			fthalf[i:i+atomW] = f
				
		fthalf + forces.forceLJMix(q,atomicity,sigma,eps,L)
	else:
		fthalf = F
		
	qn = q + delT*v + delT**2/(2*m[:,np.newaxis])*fthalf
	#Periodic Boundary conditions
	qCom = forces.rComMix(qn,m,eMols,wMols)
	qn = qn - qCom + (qCom%L)
		
		
	ftotal = np.zeros((q.shape[0],3))
	for i in range(0,eMols*atomE,atomE):        
		fn = np.zeros((atomE,3))
		qTemp = qn[i:i+atomE]
		#Bonded potentials
		for j in range(atomE):
			#Bonded Forces
			fn[j] = fn[j] + sum(forces.bforceEth(forces.bondVec(qTemp[j],qTemp[conMat[j]]),np.array(eqbmLength[j]),np.array(kL[j])))
			#Angular forces- only looping through entries with at least two atoms
			if (len(conMat[j]) >= 2):
				#Obtaining the the combinations for the forces
				combos = list(itertools.combinations(conMat[j],2))
				for k in range(len(combos)):
					#Order of atoms for the angular force computation= a1-aj-a2
					[ftj,ft0,ft1] = forces.bondedAngularForceEth(qTemp[j],qTemp[combos[k][0]],qTemp[combos[k][1]],eqbmAngle[j][k],np.array(kT[j][k]))
					fn[j] = fn[j] + ftj
					fn[combos[k][0]] = fn[combos[k][0]] + ft0
					fn[combos[k][1]] = fn[combos[k][1]] + ft1


		ftotal[i:i+atomE] = fn
		
		
	for i in range(eMols*atomE,q.shape[0],atomW):
		#Angular Force Computation- new timeStep
		AngF = forces.bondedAngularForce(qn[i:i+atomW],kTW,eAngleW)
		#Bonded Force Computation- new timeStep
		ft = np.zeros((atomW,3))
		ft[1] = forces.bforce(forces.bondVec(qn[i],qn[i+1]),eLengthW,kLW) + AngF[1]
		ft[0] = forces.bforce(forces.bondVec(qn[i+1],qn[i]),eLengthW,kLW) + forces.bforce(forces.bondVec(qn[i+2],qn[i]),eLengthW,kLW) + AngF[0]
		ft[2] = forces.bforce(forces.bondVec(qn[i],qn[i+2]),eLengthW,kLW) + AngF[2]
		ftotal[i:i+atomW] = ft

	#Integration- velocities
	ftotal = ftotal + forces.forceLJMix(qn,atomicity,sigma,eps,L)
	vn = v+ (delT/(2*m[:,np.newaxis]))*(fthalf + ftotal)

	return [qn,vn,ftotal]

