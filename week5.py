# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 16:44:22 2017

@author: s146152
"""

import numpy as np
import matplotlib.pyplot as plt

# Constants (H2O)
molecules = 2; atoms = 3
names = ['O', 'H', 'H']
connect = np.array([[0,1],[0,2]])
cons = np.size(connect, axis=0)
count1 = 1
while count1 < molecules:
    connect = np.vstack((connect, connect+3*count1))
    count1 += 1
kA = 0.01
kB = 1
eps = 1
sig = 2
equiDist = 1
equiAngle = 1.85

mass = np.array([16, 1, 1])


# Parameters algorithms
dt = 0.1 #step size
steps = 1000 #amount of steps


# Initial conditions
q_init = np.array([[0, 0.1, 0],
     [-1.2, 0, 0],
     [1.1, 0, 0]])
q_init = np.vstack((q_init, q_init+[5,0,0]))
v_init = np.array([[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]])
v_init = np.vstack((v_init,v_init))


# Velocity Verlet
q = q_init
v = v_init

file2 = open('WaterMolecules.xyz', 'w')
file2.write('%s\n' % (atoms*molecules))
file2.write('Water t= 0\n')
for i in range(atoms*molecules):
    file2.write('%s %8s %8s %8s\n' % (names[i%atoms],q[i,0],q[i,1],q[i,2]))
    
diff = q-q[:,np.newaxis]
r = np.linalg.norm(diff, axis=2)
diff_bnd = np.reshape(diff[connect[:,0],connect[:,1]], (molecules, cons, 3))
r_bnd = np.linalg.norm(diff_bnd,axis=2)
angle = np.arccos(np.diag(np.inner(diff_bnd[:,0], diff_bnd[:,1]))/np.prod(r_bnd, axis=1))

FB = -kB*(r_bnd[:,:,np.newaxis]-equiDist)*diff_bnd/r_bnd[:,:,np.newaxis]
FA = kA*(angle[:,np.newaxis]-equiAngle)*(diff_bnd[:,::-1]/np.prod(r_bnd,axis=1)[:,np.newaxis]-diff_bnd/(r_bnd[:,:,np.newaxis]**2)*np.cos(angle[:,np.newaxis]))/np.sin(angle[:,np.newaxis])








FLJ = np.zeros((atoms*molecules,atoms*molecules,3))
for i in range(atoms*molecules):
    for j in range(atoms*(i//atoms+1),atoms*molecules):
        temp = (sig/r[i,j])**6
        FLJ[j,i] = -4*eps*((6/r[i,j]**2)*temp*diff[i,j]-(12/r[i,j]**2)*(temp**2)*diff[i,j])
FLJ = FLJ-np.transpose(FLJ, (1,0,2))
Fnew = np.sum(FLJ, axis=1)









for i in range(molecules):
    Fnew[3*i] = Fnew[3*i]-np.sum(FB[i],axis=0)-np.sum(FA[i],axis=0)/3
    Fnew[3*i+1:3*i+3] = Fnew[3*i+1:3*i+3]+FA[i]+FB[i]-np.sum(FA[i],axis=0)/3

for t in range(steps):
    time = (float(t)+1.0)*dt
    F = Fnew
    q = q + dt*v + (dt**2/2)*F/np.tile(mass[:,np.newaxis],(molecules,1))
    
    diff = q-q[:,np.newaxis]
    r = np.linalg.norm(diff, axis=2)
    diff_bnd = np.reshape(diff[connect[:,0],connect[:,1]], (molecules, cons, 3))
    r_bnd = np.linalg.norm(diff_bnd,axis=2)
    angle = np.arccos(np.diag(np.inner(diff_bnd[:,0], diff_bnd[:,1]))/np.prod(r_bnd, axis=1))

    FB = -kB*(r_bnd[:,:,np.newaxis]-equiDist)*diff_bnd/r_bnd[:,:,np.newaxis]
    FA = kA*(angle[:,np.newaxis]-equiAngle)*(diff_bnd[:,::-1]/np.prod(r_bnd,axis=1)[:,np.newaxis]-diff_bnd/(r_bnd[:,:,np.newaxis]**2)*np.cos(angle[:,np.newaxis]))/np.sin(angle[:,np.newaxis])

    FLJ = np.zeros((atoms*molecules,atoms*molecules,3))
    for i in range(atoms*molecules):
        for j in range(atoms*(i//atoms+1),atoms*molecules):
            temp = (sig/r[i,j])**6
            FLJ[j,i] = -4*eps*((6/r[i,j]**2)*temp*diff[i,j]-(12/r[i,j]**2)*(temp**2)*diff[i,j])
    FLJ = FLJ-np.transpose(FLJ, (1,0,2))
    Fnew = np.sum(FLJ, axis=1)

    for i in range(molecules):
        Fnew[3*i] = Fnew[3*i]-np.sum(FB[i],axis=0)-np.sum(FA[i],axis=0)/3
        Fnew[3*i+1:3*i+3] = Fnew[3*i+1:3*i+3]+FA[i]+FB[i]-np.sum(FA[i],axis=0)/3
    
    v = v + (dt/2)*(F/np.tile(mass[:,np.newaxis],(molecules,1))+Fnew/np.tile(mass[:,np.newaxis],(molecules,1)))
    file2.write('%s\n' % (atoms*molecules))
    file2.write('Water t= %s\n' % time)
    for i in range(atoms*molecules):
        file2.write('%s %8s %8s %8s\n' % (names[i%atoms],q[i,0],q[i,1],q[i,2])) 

file2.close
