import numpy as np

def bondVec(q1,q2):
	#Returns the bond vector 
	return q1-q2
	
def angle(x1,x2):
	#Computes the angle b/w two vectors in radians
	return (np.arccos(np.dot(x1,x2)/(np.linalg.norm(x1)*np.linalg.norm(x2))))
 
def nbDistance(q,localPos,molNumber,atomicity,L):
    #Returns the non-bonded atom distances
	dr = q - q[(atomicity*molNumber)+localPos]
	dr - dr - L*np.floor(dr/L + 0.5)
	nbDist = np.zeros(q.shape - np.asarray([atomicity,0]))
	it = 0
	for i in range(dr.shape[0]):
		if (np.floor(i/atomicity) != molNumber):
			nbDist[it] = dr[i]
			it = it+1
	"""for i in range(localPos,-1,-1):
		nbDist = np.delete(dr,molNumber+i,axis = 0)
	for i in range(localPos+1,atomicity,1):
		nbDist = np.delete(dr,molNumber+i,axis = 0)"""	
	#Here we neglect the non-bonded atoms of the same molecule
	#Returns the bond Distances for the ith atom- an array of size=(NAtoms - atomicity)
	return nbDist
	
def bforce(bondVector, eqbmLength, k):
	#returns the bonded force (for water)
	return k*(np.linalg.norm(bondVector) - eqbmLength)*bondVector/np.linalg.norm(bondVector)
		
def bforceEth(bondVector,eqbmLength,k):
	#Returns the bonded force for ethanol
    normVec = (np.divide(bondVector.T,np.linalg.norm(bondVector,axis=1))).T
    force = -k*(np.linalg.norm(bondVector,axis=1) - eqbmLength)
    for i in range(normVec.shape[0]):
    	normVec[i] = force[i]*normVec[i]
    return normVec
    
def bondedPotentialEthanol(q,acList,eqbmLength,k):
	#Computes the bond potential for ethanol
    pot = 0
    for i in range(len(acList)):
    	pot = pot + 0.5*k[acList[i][0]][acList[i][1]]*(round(np.linalg.norm(q[acList[i][0]] - q[acList[i][1]]),3) - eqbmLength[acList[i][0]][acList[i][1]])**2
    return pot

def bpot(bondVector,eqbmLength,k):
	#Returns the bond potential (for water)
    pot = k/2*(np.linalg.norm(bondVector) - eqbmLength)**2
    return pot
 
def bondedAngularForceEth(r1,r2,r3,eqangle,k):
    #Computing bond forces b/w a group of atoms= r2-r1-r3(a-b-c)
	bv1 = -bondVec(r1,r2)#ba
	bv2 = -bondVec(r1,r3)#bc
	#Converting Eqbm angle into radians
	eqangle = (np.pi/180)*eqangle
	pa = (np.cross(bv1,np.cross(bv1,bv2)))/np.linalg.norm(np.cross(bv1,np.cross(bv1,bv2)))
	fa = ((-2*k*(angle(bv1,bv2) - eqangle))/(np.linalg.norm(bv1)))*pa
	pc = (np.cross(-bv2,np.cross(bv1,bv2)))/np.linalg.norm(np.cross(-bv2,np.cross(bv1,bv2)))
	fc = ((-2*k*(angle(bv1,bv2) - eqangle))/(np.linalg.norm(bv2)))*pc
	fb = -(fa + fc)
	return [fb,fa,fc]
	
def bondedAngularPotentialEthanol(r1,r2,r3,eqangle,k):
	#Computing bond potentials b/w a group of atoms= r2-r1-r3(a-b-c)
	bv1 = -bondVec(r1,r2)#ba
	bv2 = -bondVec(r1,r3)#bc
	#Converting Eqbm angle into radians
	eqangle = (np.pi/180)*eqangle
	potential = 0.5*k*(angle(bv1,bv2)-eqangle)**2
	return potential
 
 
def bondedAngularForce(r,k,eqangle):
	#Computes the angular forces b/w a group of three atoms - r1-r0-r2.
	bv1 = bondVec(r[1],r[0])
	bv2 = bondVec(r[2],r[0])
	pa = (np.cross(bv1,np.cross(bv1,bv2)))/np.linalg.norm(np.cross(bv1,np.cross(bv1,bv2)))
	fa = ((-2*k*(angle(bv1,bv2) - eqangle))/(np.linalg.norm(bv1)))*pa
	pc = (np.cross(bv2,np.cross(bv2,bv1)))/np.linalg.norm(np.cross(bv2,np.cross(bv2,bv1)))
	fc = ((-2*k*(angle(bv1,bv2) - eqangle))/(np.linalg.norm(bv2)))*pc
	fb = -(fa + fc)
	return [fb,fa,fc]
	
def nbforce(dist,sigma,epsilon):
	#Computes the non-bonded forces - Lennard Jones potential forces for the atoms
	f = np.array([0.0,0.0,0.0])
	for i in range(dist.shape[0]):
		r = np.linalg.norm(dist[i])
		#Add a cut-off length 
		f = f + (24*epsilon/r)*(2*(sigma/r)**12 - (sigma/r)**6)*(dist[i]/r)
	return f
	
def lennardJonesPotential(d,sigma,epsilon):
	#Calculates the Lennard Jones potential
    r = np.linalg.norm(d)
    type(r)
    print(r)
    return 4*epsilon*((sigma/r)**12 - (sigma/r)**6)
	
def tforce(q1,q2,q3,q4,fparams,L):
	#Computes the torsional forces between 4 non-planar atoms of a molecule- Ryckaert Bellman Potentials
	f = np.zeros((4,3))
	ba = bondVec(q1,q2)
	bc = bondVec(q3,q2)
	cd = bondVec(q4,q3)
	oc = bc/2.0
	p1 = np.cross(ba,bc)/np.linalg.norm(np.cross(ba,bc))
	p2 = np.cross(bc,cd)/np.linalg.norm(np.cross(bc,cd))
	theta = np.arccos(np.dot(p1,p2)/(np.linalg.norm(p1)*np.linalg.norm(p2)))
	#theta = np.arctan2(np.cross(p1,p2),np.dot(p1,p2))
	theta_abc = np.arccos(np.dot(ba,bc)/(np.linalg.norm(ba)*np.linalg.norm(bc)))
	#theta_abc = np.arctan2(np.cross(ba,bc),np.dot(ba,bc))
	theta_bcd = np.arccos(np.dot(cd,bc)/(np.linalg.norm(cd)*np.linalg.norm(-bc)))
	#theta_bcd = np.arctan2(np.cross(bc,cd),np.dot(-bc,cd))#or np.dot(-bc,cd)
	
	#tforce is derivative of potential, so calculate dUdtheta*dthetadx
	dUdtheta = fparams[0]*np.sin(theta)-2*fparams[1]*np.sin(2*theta)+3*fparams[2]*np.sin(3*theta)-4*fparams[3]*np.sin(4*theta)
	f[0] = 0.5*((dUdtheta)/(np.linalg.norm(ba)*np.sin(theta_abc)))*p1
	f[3] = 0.5*((dUdtheta)/(np.linalg.norm(cd)*np.sin(theta_bcd)))*p2
	t = -(np.cross(oc,f[3]) + 0.5*np.cross(cd,f[3]) + 0.5*np.cross(ba,f[0]))
	f[2] = np.cross(t,oc)*(1/np.linalg.norm(oc)**2)
	f[1] = -(f[0]+f[2]+f[3])
	return f
	
	
def tpotential(q1,q2,q3,q4,fparams):
	#Returns the torsional potential
	ba = bondVec(q1,q2)
	bc = bondVec(q3,q2)
	cd = bondVec(q4,q3)
	p1 = np.cross(ba,bc)/np.linalg.norm(np.cross(ba,bc))
	p2 = np.cross(bc,cd)/np.linalg.norm(np.cross(bc,cd))
	theta = np.arccos(np.dot(p1,p2)/(np.linalg.norm(p1)*np.linalg.norm(p2)))
	tp = 0.5*(fparams[0]*(1+np.cos(theta))+fparams[1]*(1-np.cos(2*theta))+fparams[2]*(1+np.cos(3*theta))+fparams[3]*(1-np.cos(4*theta)))
	return tp
	
def vCom(v,mass):
	"""
	Computes the velocity of the center of mass the molecule
	v- velocity matrix - (nAtoms*3)
	"""
	atomicity = len(mass)
	nMols = int(v.shape[0]/atomicity)
	vcom = np.zeros((nMols,3))
	it = 0
	for i in range(0,v.shape[0],atomicity):
		vcom[it] = np.average(v[i:i+atomicity],axis=0,weights=np.array(mass))
		it = it + 1
	return vcom

def rCom(r,mass):
	"""
	Computes the position of the center of mass the molecule
	r- position matrix - (nAtoms*3)
	"""
	atomicity = len(mass)
	nMols = int(r.shape[0]/atomicity)
	rcom = np.zeros((nMols,3))
	it = 0
	for i in range(0,r.shape[0],atomicity):
		rcom[it] = np.average(r[i:i+atomicity],axis=0,weights=np.array(mass))
		it = it + 1
	return rcom
	
def rComMix(r,mass,eMols,wMols):
	"""
	Computes the position of the center of mass the molecule
	r- position matrix - (nAtoms*3)
	"""
	rcom = np.zeros((9*eMols+3*wMols,3))
	for i in range(0,9*eMols,9):
		rcom[i:i+9] = np.average(r[i:i+9],axis=0,weights=np.array(mass[:9]))
	for i in range(9*eMols,9*eMols+3*wMols,3):
		rcom[i:i+3] = np.average(r[i:i+3],axis=0,weights=np.array(mass[9*eMols:9*eMols+3]))
	return rcom
	
	
def kineticEnergy(v,mass):
	"""
	Computes the kinetic energy of the molecules 
	"""
	atomicity = len(mass)
	nMols = int(v.shape[0]/atomicity)
	ke = np.zeros(nMols)
	it = 0
	#v = 1000*v #Converting to regular units
	#mass = 1.67e-27*mass
	for i in range(0,v.shape[0],atomicity):
		ke[it] = 0.5*np.dot(np.linalg.norm(v[i:i+atomicity],axis=1)**2,mass)
		#print(ke[it])
		it = it + 1
	return ke
	
	
def angMom(r,v,mass):
	"""
	Computes the angular momentum of the molecule
	"""
	atomicity = len(mass)
	nMols = int(r.shape[0]/atomicity)
	rcom = rCom(r,mass)
	am = np.zeros((nMols,3))
	it = 0
	for i in range(0,r.shape[0],atomicity):
		am[it] = sum(np.cross(r[i:i+atomicity]-rcom,np.dot(np.array(mass),v[i:i+atomicity])))
		it = it + 1
	return am

def forceLJ(q,atomicity,sig,eps,L):
# =============================================================================
#     This might work for a homogeneous mixture, but will be difficult to adapt to water-ethanol mixture
# =============================================================================
    #Calculates the Lennard Jones forces
	diff = q-q[:,np.newaxis]
	diff = diff - L*np.floor(diff/L + 0.5)
	r = np.linalg.norm(diff,axis=2)
	N = np.shape(q)[0]
	FLJ = np.zeros((N,N,3))
	for i in range(N):
		for j in range(atomicity*(i//atomicity+1),N):
			if (r[i,j] < 8): #Cutoff length is 0.8 nm
				sigma = 0.5*sig[i%atomicity]+0.5*sig[j%atomicity]
				epsilon = np.sqrt(eps[i%atomicity]*eps[j%atomicity])
				temp = (sigma/r[i,j])**6
				FLJ[j,i] = -4*epsilon*((6/r[i,j]**2)*temp*diff[i,j]-(12/r[i,j]**2)*(temp**2)*diff[i,j])
	FLJ = FLJ-np.transpose(FLJ, (1,0,2))
	return np.sum(FLJ,axis=1)
	
	
def forceLJMix(q,atomicity,sig,eps,L):
# =============================================================================
#     This might work for a homogeneous mixture, but will be difficult to adapt to water-ethanol mixture
# =============================================================================
    
    diff = q-q[:,np.newaxis]
    diff = diff - L*np.floor(diff/L + 0.5)
    r = np.linalg.norm(diff,axis=2)
    N = np.shape(q)[0]
    
    FLJ = np.zeros((N,N,3))
    for i in range(N):
        for j in range(atomicity[i]*(i//atomicity[i]+1),N):
            if (r[i,j] < 8): #Cutoff length is 0.8 nm
                sigma = 0.5*sig[i]+0.5*sig[j]
                epsilon = np.sqrt(eps[i]*eps[j])
                temp = (sigma/r[i,j])**6
                FLJ[j,i] = -4*epsilon*((6/r[i,j]**2)*temp*diff[i,j]-(12/r[i,j]**2)*(temp**2)*diff[i,j])
    FLJ = FLJ-np.transpose(FLJ, (1,0,2))
    
    return np.sum(FLJ,axis=1)	
	
	
def temperature(v,k,m):
# =============================================================================
#     vr = np.var(v)
#     T = (sum(m)/k)*vr
#     T = T*120.27
# =============================================================================
    # Calculates the temperature from the kinetic energy in the system (report)
	N = np.shape(v)[0]
	mass = np.tile(m,(np.shape(v)[1],int(N/len(m)))).T
	T = (1/(3*k*N))*np.sum(mass*v**2)
	return T
	
def temperatureMix(v,k,m):
# =============================================================================
#     vr = np.var(v)
#     T = (sum(m)/k)*vr
#     T = T*120.27
# =============================================================================
    # Calculates the temperature from the kinetic energy in the system (report)
	N = np.shape(v)[0]
	T = (1/(3*k*N))*np.sum(mass[:,np.newaxis]*v**2)
	return T