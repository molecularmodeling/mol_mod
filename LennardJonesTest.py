# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 21:09:47 2018

@author: hemaditya

Reduced units taken.
Length = 1e-10 m
time = 0.1ps
energy = 1kj/mol 
"""

import numpy as np
import trajectoryIO
import matplotlib.pyplot as plt



def lennardJonesPotential(d,sigma,epsilon):
    r = np.linalg.norm(d)
    return 4*epsilon*((sigma/r)**12 - (sigma/r)**6)
    
def lennardJonesForce(q,sig,eps,atomicity):
# =============================================================================
#     This might work for a homogeneous mixture, but will be difficult to adapt to water-ethanol mixture
# =============================================================================
    
    diff = q-q[:,np.newaxis]
    r = np.linalg.norm(diff,axis=2)
    N = np.shape(q)[0]
    
    FLJ = np.zeros((N,N,3))
    for i in range(N):
        for j in range(atomicity*(i//atomicity+1),N):
            sigma = 0.5*sig[i%atomicity]+0.5*sig[j%atomicity]
            epsilon = np.sqrt(eps[i%atomicity]*eps[j%atomicity])
            temp = (sigma/r[i,j])**6
            FLJ[j,i] = -4*epsilon*((6/r[i,j]**2)*temp*diff[i,j]-(12/r[i,j]**2)*(temp**2)*diff[i,j])
    FLJ = FLJ-np.transpose(FLJ, (1,0,2))
    
    return np.sum(FLJ,axis=1)
    
    
    
def velVerletIntLPJ(q,v,nAtoms,m,eps,sigma,tFinal,delT,tStart=0):
    qn = np.zeros(q.shape)    
    vn = np.zeros(v.shape)
    
    #Forces
    f = lennardJonesForce(q,sigma,eps,1)
    
    #Positions
    qn = q + delT*v + ((delT**2)/2*m[:,np.newaxis])*f
    
    #new forces
    ft = lennardJonesForce(qn,sigma,eps,1)
    
    
    #Velocities
    vn = v + ((delT)/(2*m[:,np.newaxis]))*(f+ft)
    
    
    return [qn,vn,f,ft]

m = np.array([16,16])
q = [[1,0,0],[-1,0,0]] # Bond length of O2 is 1.48A(taken from the internet :p)
v = [[0,0,0],[0,0,0]]
nAtoms = 2
tFinal = 400
delT = 0.01
eps = np.array([3.15061])
sigma = np.array([0.66386])
q = np.array(q)
v = np.array(v)

bondsO = ['O','O']



print('Start simulation!')
timeSteps = int((tFinal-0.0)/delT)
p = np.zeros((timeSteps))
dr = np.zeros(timeSteps)
force = np.zeros(timeSteps)
forcen = np.zeros(timeSteps)
#Integration
for i in range(timeSteps):
	print("Time step: ",i,"\n")
	file = trajectoryIO.writeTrajectory('wrLPJ.xyz',q,nAtoms,i*delT,timeSteps,bondsO,file)
	[q,v,f,ft] = velVerletIntLPJ(q,v,nAtoms,m,eps,sigma,tFinal,delT)
	force[i] = np.linalg.norm(f[0])
	forcen[i] = np.linalg.norm(ft[0])
	p[i] = lennardJonesPotential(q[0],sigma,eps)
	dr[i] = np.linalg.norm(q[0]-q[1])
plt.plot(dr)
plt.hold
plt.plot(forcen)
plt.show()
